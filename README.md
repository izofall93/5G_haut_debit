![](capture/image2.jpg)

REPUBLIQUE DU SENEGAL

UN PEUPLE-UN BUT-UNE FOI

MINISTERE DE L'ENSEIGNEMENT SUPERIEUR
DIRECTION GENERALE DE L'ENSEIGNEMENT SUPERIEUR

![](capture/image3.png) 

                                **[Réseaux haut débit]{.ul}**

                            MASTER 2 : RESEAUX ET TELECOMMUNICATIONS

-   **[Présenté par]{.ul}** :                       **[Sous la direction de]{.ul}**

        *Ismaïla FALL*                                           *Mr Samuel OUYA*

        *Amadou NGAÏDE*

        *Laurelle*

        *Maodo Malick MBAYE*

        *Wilian CO*

        *Ibrahima BA*

                                            Juillet 2022

# 

# 

# 

# 

**Objectif**

Ce rapport est une présentation générale de la 5G abordée sous l'angle
du déploiement. Il s'inscrit dans le cadre d'une mise en place d'un
réseau 5G SA avec Open5GS et 5G NSA avec srsRAN qui sont des logiciels open source
de réseau central mobile 5G (SA et NSA). Pour tester les fonctions du
réseau central 5G, on a utilisé UERANSIM un simulateur open source 5G UE
et RAN (gNodeB).

# **Introduction**

Le secteur des télécoms utilise très souvent le terme \'**Très Haut
Débit**\' pour désigner les réseaux internet mobiles ou fixes permettant
l\'envoi et la réception d\'un grand volume de données en un très court
laps de temps. Concrètement, on parle de Très Haut Débit quand un réseau
a un **débit supérieur à 30 Mbps**. On peut alors dresser une liste des
réseaux Très Haut Débit :

-   La **3G**, d\'un débit maximum théorique de **42 Mbps**

-   La **4G**, d\'un débit maximum théorique de **112,5 Mbps**

-   La **4G+**, évolution du réseau 4G, d\'un débit maximum théorique
    de **187,5 Mbps**

-   La **5G**, dernière génération de réseau internet mobile, d\'un
    débit maximum théorique **dépassant les 1 Gbps**.** **

Le présent document se penchera sur la 5G (la technologie sans fil de
cinquième génération pour les réseaux mobiles) d'un point de vue
technique.

# **Qu'est-ce que la 5G ?** 

La 5G, ou 5G NR (New Radio) est, comme son nom l'indique, la 5e
génération de communications mobiles qui succède à la 4G LTE, et avant
elle la 3G et la 2G. Parmi les promesses phares de la 5G, on retrouve
d'abord un débit multiplié par 10, mais aussi une latence fortement
réduite, divisée par 10.

La 5G est Considérée comme un véritable \" facilitateur \" de la
numérisation de la société, en autorisant le développement de nouveaux
usages : réalité virtuelle, véhicule autonome et connecté, ville
intelligente (contrôle du trafic routier, optimisation énergétique),
industrie du futur (pilotage à distance des outils industriels,
connectivité des machines etc).

# **Réseau cellulaire 5G : Architecture** 

Le réseau cellulaire se compose de deux sous-systèmes principaux : le
réseau d\'accès radio (RAN) et le noyau mobile (5G Core Network).

Le RAN gère le spectre radio, en s\'assurant qu\'il est utilisé
efficacement et répond aux exigences de qualité de service de chaque
utilisateur. Il correspond à un ensemble distribué de stations de base.
En 5G, ils sont connus sous le nom de gNB.

![](capture/image4.png)
*Réseau d\'accès radio 5G*


Le cœur de la 5G utilise une architecture basée sur les services (SBA)
alignée sur le cloud pour prendre en charge l\'authentification, la
sécurité, la gestion des sessions et l\'agrégation du trafic des
appareils connectés, le tout nécessitant l\'interconnexion complexe des
fonctions du réseau, comme le montre le schéma du cœur de la 5G.

![](capture/image5.png)

*Réseau central 5G*

On peut regrouper l\'ensemble des blocs fonctionnels en trois groupes.

Le premier groupe s\'exécute dans le plan de contrôle (CP).

-   AMF (Core Access and Mobility Management Function) : Responsable de
    la gestion de la connexion et de l\'accessibilité, de la gestion de
    la mobilité, de l\'authentification et de l\'autorisation des accès
    et des services de localisation. Gère les aspects liés à la mobilité
    du MME de l\'EPC.

-   SMF (fonction de gestion de session) : gère chaque session UE, y
    compris l\'attribution d\'adresse IP, la sélection de la fonction UP
    associée, les aspects de contrôle de la QoS et les aspects de
    contrôle du routage UP. Correspond approximativement à une partie de
    la MME de l\'EPC et aux aspects liés au contrôle de la PGW de
    l\'EPC.

-   PCF (Policy Control Function) : gère les règles de politique que
    d\'autres fonctions CP appliquent ensuite. Correspond à peu près au
    PCRF de l\'EPC.

-   UDM (Unified Data Management) : gère l\'identité de l\'utilisateur,
    y compris la génération des identifiants d\'authentification. Inclut
    une partie de la fonctionnalité dans le HSS de l\'EPC.

-   AUSF (Authentication Server Function) : Essentiellement un serveur
    d\'authentification. Inclut une partie de la fonctionnalité dans le
    HSS de l\'EPC.

Le deuxième groupe s\'exécute également dans le plan de contrôle (CP)
mais n\'a pas d\'équivalent direct dans l\'EPC :

-   SDSF (Structured Data Storage Network Function) : Service « helper »
    utilisé pour stocker des données structurées.

-   UDSF (Unstructured Data Storage Network Function) : Un service «
    d\'assistance » utilisé pour stocker des données non structurées.

-   NEF (Network Exposure Function) : moyen d\'exposer certaines
    fonctionnalités à des services tiers, y compris la traduction entre
    les représentations internes et externes des données.

-   NRF (NF Repository Function) : Un moyen de découvrir les services
    disponibles.

-   NSSF (Network Slicing Selector Function) : un moyen de sélectionner
    une tranche de réseau pour desservir un UE donné. Les tranches de
    réseau sont essentiellement un moyen de partitionner les ressources
    du réseau afin de différencier le service fourni aux différents
    utilisateurs. Il s\'agit d\'une caractéristique clé de la 5G.

Le troisième groupe comprend le seul composant qui s\'exécute dans le
plan utilisateur (transporte le trafic utilisateur du réseau) :

-   UPF (User Plane Function) : transfère le trafic entre RAN et
    Internet, correspondant à la combinaison S/PGW dans EPC. En plus du
    transfert de paquets, il est responsable de l\'application des
    politiques, de l\'interception légale, des rapports sur
    l\'utilisation du trafic et de la régulation de la qualité de
    service.

Éléments de réseau 4G EPC avec les fonctions de réseau central 5G :

![](capture/image6.png)

# **Différence entre la 4G et la 5G**

La 4G et la 5G : quelles différences ?

Alors que la 4G permettait déjà de surfer sur internet à grande vitesse,
la 5G pulvérise tous les records en matière de rapidité. Le débit est
très nettement amélioré : jusqu\'à 10 fois plus rapide que la 4G, grâce
à l\'utilisation de la nouvelle bande 5G (3,5 Ghz). Autrement dit, les
échanges ou le visionnage de fichiers de toutes sortes, y compris les
fichiers très lourds comme les vidéos en HD, UHD ou 4K, sont quasi
instantanés avec la 5G. La navigation internet, le streaming ou encore
le cloud gaming sont tous rendus ultra fluides grâce à la 5G. Et
l\'utilisation de la bande 3,5 Ghz permet d\'assurer une couverture
optimale du territoire.

Globalement, la 5G est nettement plus rapide, plus performante et plus
intelligente que la 4G. C\'est un gage de confiance pour les
utilisateurs, et une source de fiabilité et d\'innovations infinies pour
les entreprises.

Une brève comparaison des technologies 5G et 4G est donnée dans le
tableau ci-dessous

  **Technologie** |  **Débits de données**  |    **Latence**        |       **Densité d\'utilisateurs**
  ----------------- |--------------------------| -------------------------| -----------------------------
  5G (NR)           |  Moy 100 Mb/s Pic 20 Gb/s  |  \~ 1 milliseconde       |   1000K/Km carré
  4G (LTE)          | Moy 25 Mb/s Pic 300 Mb/s  |  \~10- 50 millisecondes  | \~ 2K / Km carré

# **Les fréquences utilisées pour la 5G**

Les fréquences utilisées en 5G sont regroupées en deux grands ensembles
: le groupe FR1 avec les basses fréquences 5G sub-6, sous les 6 GHz, et
le groupe FR2 avec les hautes fréquences mmWave avec une longueur d'onde
de l'ordre du millimètre.

![](capture/image7.png)

*Spectre des fréquences 5G*

En tout, la 5G telle que définie par le 3GPP intègre un peu moins de 50
bandes de fréquences différentes pour le premier groupe FR1. Le deuxième
groupe est beaucoup plus maigre et ne comprend que 4 bandes de
fréquences différentes.

Au Sénégal, voici les bandes de fréquences qui sont utilisées pour
tester la 5G sub-6 :

**n28 : 700 MHz**

À cela s'ajoutent les bandes de fréquences mmWave :

**n258 : 26 GHz**

Et les bandes de fréquence 4G LTE réutilisées pour la 5G :

**n1 : 2100 MHz**

**n3 : 1800 MHz**

**n7 : 2600 MHz**

**n20 : 800 MHz**

**n28 : 700 MHz**

Notons que les bandes NR sont définies avec le préfixe \"n\"

# **Modes de déploiement de la 5G**

Les opérateurs de réseaux mobiles (MNOs) ont le choix entre deux options
principales lors du déploiement de la 5G : non autonome (NSA) et
autonome (SA).

Le déploiement de la 5G peut se faire en plusieurs phases. Certains pays
ont commencé à déployer leur réseau en utilisant la 5G NSA, ou 5G
Non-standalone.

## **V.1. 5G NSA (non autonome)**

Il s'agit de continuer d'utiliser le cœur de réseau 4G LTE de
l'opérateur tout en ajoutant petit à petit des antennes 5G, et permettre
notamment l'utilisation de hautes fréquences en 5G NR.

Dans la NSA, le trafic du plan de contrôle entre l\'équipement de
l\'utilisateur et le 4G Mobile Core utilise (c\'est-à-dire est transmis
via) les stations de base 4G, et les stations de base 5G ne sont
utilisées que pour transporter le trafic de l\'utilisateur.

![](capture/image60.png)

-   Avantages de la NSA 5G

    -   Coûts réduits. Les opérateurs de réseaux mobiles (MNOs) peuvent
        construire un réseau 5G en plus de leur infrastructure 4G
        existante au lieu d\'investir dans un nouveau cœur 5G coûteux.

    -   Déploiement facile. Les réseaux NSA utilisent une infrastructure
        4G avec laquelle les MNOs sont déjà familiers, simplifiant à la
        fois les processus de configuration et de mise à jour.

    -   Déploiement rapide. Les Opérateurs peuvent libérer un réseau 5G
        opérationnel plus rapidement avec la NSA en utilisant
        l\'infrastructure 4G actuelle.

    -   Voie vers SA 5G. Les MNO ont configuré les réseaux NSA 5G comme
        base tandis que les réseaux SA se développaient. Au fur et à
        mesure du déploiement des réseaux SA, les opérateurs peuvent
        remplacer les éléments de réseau 4G obsolètes par une
        infrastructure 5G pour gérer leurs réseaux 5G existants.

## **V.2. 5G Standalone (Autonome)**

Par opposition, la 5G SA, ou 5G Standalone, représente l'idéale du
déploiement de la 5G, ou un appareil peut utiliser les technologies 5G
aussi bien sur les basses et les hautes fréquences, avec un cœur de
réseau entièrement migré vers la 5G NR. Dans cette situation, l'appareil
ne se repose plus sur les technologies de la 4G LTE. Cela demande des
investissements bien plus conséquents, et ne sera donc disponible qu'à
long terme.

![](capture/image61.png)

-   Avantages de 5G SA

    -   Consommation électrique réduite. Parce que SA n\'a pas besoin de
        fonctionner avec 4G LTE, il n\'utilise qu\'une seule méthode de
        connectivité cellulaire et utilise moins d\'énergie pour prendre
        en charge un réseau.

    -   Prend en charge plus de cas d\'utilisation 5G. Contrairement à
        NSA, SA peut fournir des services 5G essentiels, tels que
        l\'amélioration de la latence et l\'augmentation des plafonds de
        bande passante, pour alimenter des réseaux ultrarapides et
        évolutifs.

# **Mise en pratique : déploiement d'un réseau 5G**

## **VI.1. Déploiement d'un réseau 5G SA avec open5Gs et UERANSIM**

Dans ce scénario, on a déployé le réseau 5G Core sur une machine Ubuntu
18.04 avec Open5GS et la simulation gNB/UE sur une autre machine Ubuntu
18.04 avec UERANSIM et on aussi une troisième machine Ubuntu avec
UERANSIM (simulant un UE seulement). Open5GS déployé sur un serveur (IP
192.168.1.28) et UERANSIM déployé sur les deux autre serveur (IP
192.168.1.30 et 192.168.1.31). L\'architecture du déploiement décrit
dans la figure suivante.

![](capture/image10.png)

*Architecture*

### **VI.1.1. Open5Gs**

Open5GS est une implémentation open source du réseau central mobile 5G.
Dans cette partie du projet, on va mettre en œuvre le réseau 5G Core
défini dans la version 3GPP. Actuellement, il prend en charge la version
16 du 3GPP en fournissant des fonctions de réseau 5G Core (AMF,
SMF+PGW-c, UPF+PGW-u, PCF, UDR, UDM, AUSF, NRF) et des fonctions de
réseau Evolved Packet Core( MME, SGW-c, SGW-u, HSS, and PCRF).

![](capture/image11.png)

*Architecture open5Gs*

Le cœur 5G SA fonctionne différemment du cœur 4G - il utilise une
architecture basée sur les services (SBI). Les fonctions du plan de
contrôle sont configurées pour s\'enregistrer auprès de la NRF, et la
NRF les aide ensuite à découvrir les autres fonctions principales.
Traversant les autres fonctions : L\'AMF assure la gestion de la
connexion et de la mobilité ; un sous-ensemble de ce dont le MME 4G est
chargé. Les gNB (stations de base 5G) se connectent à l\'AMF. L\'UDM,
l\'AUSF et l\'UDR effectuent des opérations similaires à celles du HSS
4G, générant des vecteurs d\'authentification SIM et conservant le
profil de l\'abonné. La gestion des sessions est entièrement gérée par
la SMF (précédemment sous la responsabilité de la 4G MME/SGWC/PGWC). Le
NSSF fournit un moyen de sélectionner la tranche de réseau. Enfin, il y
a le PCF, utilisé pour facturer et appliquer les politiques des abonnés.

### **VI.1.2. UERANSIM**

UERANSIM est un 5G UE et un 5G RAN (gNodeB) open source. Il peut être
considéré comme un téléphone mobile 5G et une station de base en termes
simples. Il existe 3 interfaces principales dans la perspective UE/RAN :

-   Control Interface (entre le RAN et le AMF)

-   User Interface (entre le RAN et le UPF)

-   Radio Interface (entre l'UE et le RAN).

UERANSIM prend en charge l\'exécution avec les réseaux Open5GS et
Free5GC 5G Core. Nous pouvons connecter UERANSIM à l\'un de ces réseaux
5G Core et tester la fonctionnalité.

![](capture/image12.png)

*Environnement 5G expérimental d\'un réseau mobile 5G (UERANSIM +
Open5GS*)

### **VI.1.3. Installation et configuration de Open5GS**

-   **Installation Open5GS**

Open5GS peut être installé en tant que services démon Linux natifs. Il
est également disponible pour être déployé avec Docker et Kubernetes. On
l'a déployé en tant qu\'application de service démon Linux native. Lors
de son exécution, le réseau 5G fonctionne en tant que service de démon
Linux.

Voici la façon d\'installer Open5GS sur server1.

**sudo apt update**

**apt-get install make g++ libsctp-dev lksctp-tools iproute2 sudo
add-apt-repository ppa:open5gs/latest**

**sudo apt update**

**sudo apt install open5gs**

![](capture/image13.PNG)

-   **Configuration Open5GS**

On a déployé 5G Core et gNB sur des serveurs séparés. On doit donc
configurer le NGAP adresse de liaison du AMF (IP du serveur en cours
d\'exécution 5G Core) et le GTPU adresse de liaison du UPF(IP du serveur
en cours d\'exécution 5G Core).

-   AMF

On modifie **/etc/open5gs/amf.yaml** pour définir l\'adresse IP NGAP,
l\'ID PLMN, le TAC et le NSSAI.

![](capture/image14.PNG)

-   UPF

On modifie le fichier **/etc/open5gs/upf.yaml** pour définir l\'adresse
GTP-U

![](capture/image15.PNG)
Après avoir modifié les fichiers de configuration, veuillez redémarrer
les démons Open5GS.

**\$ sudo systemctl restart open5gs-amfd**

**\$ sudo systemctl restart open5gs-upfd**

Vérifions que les services ont bien démarrés

![](capture/image16.PNG)

Ensuite, seuls gNB et UE peuvent se connecter au réseau 5G Core.

-   **Enregistrement des UE (WebGui open5gs)**

L'interface Web permet de modifier de manière interactive les données
des abonnés. Bien qu\'il ne soit pas essentiel de l\'utiliser, elle
facilite les choses lorsque vous débutez dans l\'aventure Open5GS. (Un
outil en ligne de commande est disponible pour les utilisateurs avancés)

Node.js est requis pour installer le WebUI d\'Open5GS

**\$ sudo apt update**

**\$ sudo apt install curl**

**\$ curl -fsSL https://deb.nodesource.com/setup_14.x \| sudo -E bash
-**

**\$ sudo apt install nodejs**

![](capture/image17.PNG)

On peut maintenant installer WebUI d\'Open5GS.

**\# curl -fsSL https://open5gs.org/open5gs/assets/webui/install \| sudo
-E bash
-**![](capture/image18.PNG)

Maintenant pour inscrire un abonné on se connecte sur
[**http://localhost:3000**](http://localhost:3000) avec le compte
administrateur .

Nom d\'utilisateur : admin

Mot de passe : 1423

![](capture/image19.PNG)

Pour ajouter des informations de l\'abonné, on va effectuer les
opérations WebUI dans l\'ordre suivant :

Allez dans SubscriberMenu.

Cliquez sur **ADD A
SUBSCRIBER**.![](capture/image20.PNG)

On remplit juste le champs IMSI, le contexte de sécurité (K, OPc, AMF)
et l\'APN de l\'abonné sont laissés par défaut.

![](capture/image21.PNG)

Et on clique sur **SAVE**

On va créer deux abonnés pour nos deux machines UERANSIM :

![](capture/image22.PNG)

-   **Activation du NAT**

Afin de faire le pont entre l\'UPF 5G Core et le WAN (Internet), on doit
activer le transfert IP et ajouter une règle NAT aux tables IP. Voici la
redirection de port NAT qu'on effectuée. Sans ce transfert de port, la
connectivité de 5G Core à Internet ne fonctionnerait pas.

Pour activer le transfert et ajouter la règle NAT, saisissez

Activation IPv4/IPv6 Forwarding

**echo 1 \> /proc/sys/net/ipv4/ip_forward**

**sysctl -p**

![](capture/image23.PNG)

Ajout des règles NAT

**\$ sudo iptables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o ogstun -j
MASQUERADE**

**\$ sudo ip6tables -t nat -A POSTROUTING -s 10.45.0.0/16 ! -o enp0s3 -j
MASQUERADE**

![](capture/image23.PNG)

### **VI.1.4. Installation et configuration UERANSIM**

-   **Installation**

On va installer le UERANSIM sur les deux autres machines. Sachant que le
UE de la troisième machine se connectera directement sur le gNB de la
deuxième machine pour accéder au réseau 5G.

L\'installation se fait avec **make** le fichier disponible dans le
référentiel UERANSIM.

Voici comment installer UERANSIM :

**sudo apt update**

**sudo apt upgrade**

**sudo apt install make g++ libsctp-dev lksctp-tools iproute2**

![](capture/image24.PNG)

UERANSIM ne fonctionne pas avec la version apt de cmake, c\'est pourquoi
nous devons installer snap et la version snap de cmake

**sudo snap install cmake \--classic**

Clonage et installation de UERANSIM

**git clone https://github.com/aligungr/UERANSIM**

**cd UERANSIM**

**make**

![](capture/image25.PNG)

Sortie de la commande
make
![](capture/image26.PNG)

-   **Configuration gNB**

UERANSIM contient deux parties gNB et UE. Les fichiers de configuration
gNB liés à Open5GS situés dans **UERANSIM/config/open5gs-gnb.yaml**. On
doit configurer le **linkIp**, **ngapIp**, **gtpIp** et **amfConfigs:
address** dans le fichier de configuration. linkIp, ngapIp, gtpIp sont
configurés avec server2 une adresse IP (adresse 192.168.1.30 du serveur
en cours d\'exécution UERANSIM). Le amfConfigs: address s\'agit de
l\'adresse IP (adresse IP du serveur en cours d\'exécution Open5Gs).

On doit également s'assurer que le **mcc** et le **mcn** du gNB
correspondent aux paramètres de notre AMF

![](capture/image27.PNG)

Le gNB peut être démarré avec le script **UERANSIM/build/nr-gnb** en
utilisant le fichier de configuration
**UERANSIM/config/open5gs-gnb.yaml** comme suit :

**./nr-gnb -c config/open5gs-gnb.yaml**

![](capture/image28.PNG)

On voit que le gNB s'est bien connecté

-   **Configuration des UE**

Le fichier de configuration UERANSIM UE lié à Open5GS est dans
**UERANSIM/config/open5gs-ue.yaml**. On doit configurer le
**gnbSearchList** avec l\'adresse IP du serveur2 (adresse IP du serveur
en cours d\'exécution du gNB UERANSIM).

On doit également s'assurer que le **mcc** et le **mcn** de l'UE
correspondent aux paramètres de notre AMF.

On va aussi renseigner l'IMSI définit dans Open5Gs via l'interface web
pour chaque UE

**UE 1 (machine 2) : IMSI 901700000000001 :**

![](capture/image29.PNG)

**UE 2 (machine 3) : IMSI 901700000000002 :**

![](capture/image30.PNG)

On peut démarrer les UE avec le script **UERANSIM/build/nr-ue** en
utilisant le fichier de configuration
**UERANSIM/config/open5gs-ue.yaml**.

Démarrage de
UE1![](capture/image31.PNG)

On voit au niveau du gNB que nos deux UE se sont connectés.

![](capture/image32.PNG)

### **VI.1.5. Test**

Lors de l\'exécution d\'UERANSIM UE, il crée une interface réseau
**uesimtun0**.
![](capture/image33.PNG)

Pour s'assurer que l'UE utilise open5Gs pour avoir accès à internet, on
supprime la passerelle par défaut de la machine2 qui exécute UERANSIM :

![](capture/image34.PNG)

Essayons de faire un ping sur 8.8.8.8 (DNS google) avec l'interface de
l'UE1

**ping 8.8.8.8 -I uesimtun0**

![](capture/image35.PNG)

On reçoit des réponses, alors open5Gs fonctionne correctement.

De plus, UERANSIM fournit un outil **UERANSIM/build/nr-binder** pour
lier des applications externes avec l\'interface TUN (uesimtun0). Nous
pouvons lier l\'interface **uesimtun0** à presque toutes les
applications utilisant **UERANSIM/build/nr-binder**.

On va utilsiser le navigateur Web Firefox. Par exemple :**/nr-binder
10.45.0.2 firefox**

Après avoir exécuté cette commande, tout le trafic réseau produit dans
Firefox utilisera la connectivité Internet de l\'UE.

![](capture/image36.PNG)

![](capture/image37.PNG)

On voit que la connectivité marche.

On peut tester aussi la connectivité du deuxième UE :

![](capture/image38.PNG)

Connectivité UE2 et UE1

![](capture/image39.png)

## **VI.2. Déploiement d'un réseau 5G NSA avec srsRAN**

srsRAN est une suite logicielle radio gratuite et open-source 4G et 5G.

Doté à la fois d\'applications UE et eNodeB/gNodeB, srsRAN peut être
utilisé avec des solutions de réseau central tierces pour créer des
réseaux sans fil mobiles complets de bout en bout.

La suite srsRAN comprend actuellement :

srsUE : une application full-stack 4G et 5G NSA/SA UE

srsENB : un eNodeB 4G complet avec des capacités de gNodeB 5G NSA/SA

srsEPC : une implémentation EPC 4G légère avec MME, HSS et S/P-GW

Tous les logiciels srsRAN fonctionnent sous Linux avec du matériel
informatique et radio standard.

La version 21.10 de srsRAN apporte la prise en charge de la 5G NSA à
l\'application SRS eNodeB (srsENB). Les fonctionnalités 5G NSA peuvent
être activées via les fichiers de configuration srsENB. Cette note
d\'application montre comment créer un réseau NSA 5G de bout en bout à
l\'aide de srsUE, srsENB et srsEPC. La radio virtuelle ZMQ est utilisée
à la place du matériel RF physique.

![](capture/image40.png)

*Présentation simplifiée de l\'architecture du réseau*

### **VI.2.1 Installation de Srsran** 

-   **Les prérequis**

\#**sudo apt-get install build-essential cmake libfftw3-dev
libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev
libtool autoconf libzmq3-dev**

-   **ZeroMQ**

Le logicielle srsRAN comprend une partie radio virtuelle qui utilise la
bibliothèque réseau ZeroMQ pour transférer des échantillons radio entre
les applications. L\'utilisation de ZMQ signifie qu\'il n\'y a pas
besoin de matériel RF physique.sur TCP ou IPC.

La première chose à faire est d\'installer ZeroMQ et de compiler srsRAN.
Sur Ubuntu, les bibliothèques de développement ZeroMQ peuvent être
installées avec :

sudo **apt-get install libzmq3-dev**

Alternativement, l\'installation à partir des sources peut également
être effectuée.Tout d\'abord, il faut installer **libzmq** :

**git clone <https://github.com/zeromq/libzmq.git>**

**cd libzmq**

**./autogen.sh**

![](capture/image41.PNG)

**./configure**

**make**

**sudo make install**

**sudo ldconfig**

Deuxièmement, on installe czmq :

**git clone https://github.com/zeromq/czmq.git**

**cd czmq**

**./autogen.sh**

![](capture/image42.PNG)

**./configure**

**make**

**sudo make install**

**sudo ldconfig**

Enfin, on va compiler srsRAN (en supposant qu'on a déjà installé toutes
les dépendances requises). Notons que si on a déjà construit et installé
srsRAN avant d\'installer ZMQ et d\'autres dépendances, on doit
réexécuter la commande make pour nous assurer que srsRAN reconnaît
l\'ajout de ZMQ :

**git clone https://github.com/srsRAN/srsRAN.git**

**cd srsRAN**

**mkdir build**

**cd build**

**cmake ../**

![](capture/image43.PNG)

**make** pour finir!
[](capture/image44.PNG)

Assurons-nous de lire la ligne suivante :

> **...**
>
> **\-- FINDING ZEROMQ.**
>
> **\-- Checking for module \'ZeroMQ\'**
>
> **\-- No package \'ZeroMQ\' found**
>
> **\-- Found libZEROMQ: /usr/local/include, /usr/local/lib/libzmq.so**

### **VI.2.2. Configuration de srsRAN**

Des modifications doivent être apportées aux fichiers de configuration
de srsUE et srsENB. Ces modifications permettent l\'utilisation de ZMQ
et activent également l\'opérateur 5G NSA dans srsENB. Aucune
modification de la configuration srsEPC n\'est requise.

-   **srsUE**

Pour srsUE, les modifications pertinentes doivent être apportées sur le
fichier **ue.conf** **(/home/mr120/.config/srsran/ue.conf**) pour
activer les capacités ZMQ et 5G NR. Pour activer ZMQ, les paramètres
device_name et device_args doivent être modifiés dans la section
\[**rf**\] :

**device_name = zmq**

**device_args =
tx_port0=tcp://\*:2001,rx_port0=tcp://localhost:2000,tx_port1=tcp://\*:2101,rx_port1=tcp://localhost:2100,id=ue,base_srate=23.04e6**![](capture/image45.PNG)

Ici, nous ajoutons deux canaux TX et deux canaux RX pour prendre en
charge à la fois les opérateurs primaires 4G et secondaires 5G.

Pour terminer la configuration de ZMQ, l\'espace de noms de réseau
**netns** doit être défini dans les paramètres \[**gw**\] :

![](capture/image46.PNG)

Les capacités 5G NR de l\'UE doivent également être activées dans la
configuration sous la section \[**rat.nr**\] :

![](capture/image47.PNG)

Ici, nous activons les bandes 3 et 78, qui sont respectivement les
bandes de fréquences FDD et TDD. En incluant les deux dans la
configuration UE, nous pouvons tester chaque mode duplex simplement en
configurant le réseau.

Le nombre de porteuses NR doit être défini sur 1, sinon l\'UE ne pourra
pas se connecter au gNB. Si cela n\'était pas défini, l\'UE n\'aurait
qu\'une connexion LTE.

Comme le mode NSA fait partie de la version 15 du 3GPP, cela doit être
reflété dans la configuration. La version par défaut utilisée est 8.
Ajoutons l\'entrée suivante sous le champ \[**rrc**\] :

![](capture/image48.PNG)

-   **srsENB**

Des modifications doivent être apportées à la fois **enb.conf**
(**/home/mr120/.config/srsran/enb.conf**) et **rr.conf**
**/home/mr120/.config/srsran/rr.conf**) pour activer la 5G NSA.

Configuration eNB

Tout d\'abord, les modifications requises pour activer ZMQ doivent être
apportées. Cela implique de changer le **device_name** et le
**device_args** dans la section \[**rf**\] :

**device_name = zmq**

**device_args =
fail_on_disconnect=true,tx_port0=tcp://\*:2000,rx_port0=tcp://localhost:2001,tx_port1=tcp://\*:2100,rx_port1=tcp://localhost:2101,id=enb,base_srate=23.04e6**

![](capture/image49.PNG)

Comme pour l\'UE, il existe deux canaux TX et deux canaux RX. Ces canaux
sont mappés aux ports pertinents configurés sur l\'UE.

Aucune autre modification n\'est nécessaire dans le fichier enb.conf.

-   **Configuration RRC**

La principale modification apportée au fichier **rr.conf** est l\'ajout
de la cellule NR à la liste des cellules. Ceci est ajouté à la fin du
fichier :

nr_cell_list =

(

{

rf_port = 1;

cell_id = 0x02;

tac = 0x0007;

pci = 500;

root_seq_idx = 204;

// TDD:

//dl_arfcn = 634240;

//band = 78;

// FDD:

dl_arfcn = 368500;

band = 3;

}

);

![](capture/image50.PNG)

Ici, nous avons ajouté les configurations TDD et FDD. Pour cet exemple,
nous utiliserons la configuration FDD, donc la configuration TDD est
commentée. Les configurations TDD et FDD peuvent être échangées en
arrêtant srsENB, en apportant les modifications nécessaires à ce fichier
et en redémarrant srsENB

### **VI.2.3. Démarrage et Test**

-   **Démarrage**

Avec les configurations ci-dessus, le réseau peut maintenant être
démarré. Exécutez d\'abord l\'EPC, suivi de l\'eNodeB et de l\'UE.

-   **Création de l'espace de nom ue1 par la commande suivante**

**\#ip netns add ue1**

**\#ip netns list**

![](capture/image51.PNG)

-   **Démarrage EPC**

![](capture/image52.PNG)

-   **Démarrage eNB**

![](capture/image53.PNG)

-   **Démarrage
    UE**![](capture/image54.PNG)

L\'UE a démarré et est rattaché au réseau central. L\'UE se verra
attribuer une adresse IP dans la plage configurée (par exemple
172.16.0.2).

-   **Test**

Vérifions l'adresse IP de l'ue1

\#**ip netns exec ue1 ifconfig**

![](capture/image55.PNG)

Faisons un ping vers la passerelle.

**\#ip netns exec ue1 ping 172.16.0.1**

![](capture/image57.png)

Donnons une passerelle à l'**ue1** 

**\#ip netns exec ue1 route add default gateway 172.16.0.1**

**\#ip netns exec ue1 route
-n**![](capture/image56.PNG)

Activation IPv4/IPv6 Forwarding

**echo 1 \> /proc/sys/net/ipv4/ip_forward**

**sysctl -p**

Ajout des règles NAT

**\$ sudo iptables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o srs_spgw_sgi -j
MASQUERADE**

**\$ sudo ip6tables -t nat -A POSTROUTING -s 172.16.0.0/24 ! -o enp0s3 -j
MASQUERADE**

Le trafic peut être envoyé via le réseau pour tester la connexion
internet de l'UE.

On va faire un ping sur le DNS de google
(8.8.8.8)![](capture/image58.PNG)

On voit bien que l'UE dispose d'une connexion internet.

# **Conclusion**

Dans ce projet, nous avons présenté un certain nombre d\'outil open
source de déploiement du réseau central 5G SA avec Open5GS et UERANSIM
et du réseau 5G NSA avec srsRAN. Nous avons pu décrire et mis en place
un cœur de réseau 5G (SA et NSA) fonctionnel sur lequel nous avons
connecté des utilisateurs (UE) et nous avons également réussi à faire des tests.

Nous pensons que ce travail est une première étape importante dans
l\'exploration des paradigmes de la 5G.
